/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import entities.AnswerMC;
import entities.AnswerSA;
import entities.AnswerTF;
import entities.Question;
import entities.QuestionMC;
import entities.QuestionResult;
import entities.QuestionType;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.util.ArrayList;
import static java.util.Collections.list;
import javafx.geometry.HorizontalDirection;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;

/**
 *
 * @author romina.areco
 */
public class PnlResume extends javax.swing.JPanel {

    /**
     * Creates new form pnlResume
     */
    public PnlResume() {
        initComponents();
    }
    
    public void loadQuestions(ArrayList<QuestionResult> questions){
       
        JPanel panel = new JPanel();
        panel.add(new JLabel("Resumen de respuesta"));
        
        int promedio = 0;
        
        for(int i = 0; i<questions.size(); i++){
            
            panel.add(new JLabel("   "));
                
            JSeparator separator2 = new JSeparator();
            panel.add(separator2);
    
            QuestionResult question = (QuestionResult)questions.get(i);
                
            JLabel lblQuestion = new JLabel(question.getQuestion());
            panel.add(lblQuestion);

            JSeparator separator = new JSeparator();
            panel.add(separator);
            
            promedio += question.getUsedTime();
                
            if(question.getType().equals(QuestionType.MultipleChoice)){

                panel.add(new JLabel("   "));
                panel.add(new JLabel("Respuestas correctas"));
                
                for(int j = 0; j < question.getCorrectAnswers().size(); j++){
                    AnswerMC answer = (AnswerMC)question.getCorrectAnswers().get(j);
                    panel.add(new JLabel(" • " + answer.getAnswer()));
                }
                panel.add(new JLabel("   "));
                panel.add(new JLabel("Respuestas marcadas"));
                
                for(int j = 0; j < question.getMarkedAnswers().size(); j++){
                    AnswerMC answer = (AnswerMC)question.getMarkedAnswers().get(j);
                    JLabel lblAnswer = new JLabel(" • " + answer.getAnswer());
                    panel.add(lblAnswer);
                }
                
                panel.add(new JLabel("   "));
                panel.add(new JLabel("Tiempo usado: "+question.getUsedTime()));
                
            }
            else if(question.getType().equals(QuestionType.TrueOrFalse)){
                
                panel.add(new JLabel("     "));
                panel.add(new JLabel("Respuesta correcta"));

                AnswerTF answer = (AnswerTF)question.getCorrectAnswer();
                if(answer.getAnswer()){
                    panel.add(new JLabel(" • " + "True"));
                }
                else{
                    panel.add(new JLabel(" • " + "False"));
                }

                panel.add(new JLabel("   "));
                panel.add(new JLabel("Respuesta marcada"));

                AnswerTF answer2 = (AnswerTF)question.getMarkedAnswer();
                if(answer2.getAnswer()){
                    panel.add(new JLabel(" • " + "True"));
                }
                else{
                    panel.add(new JLabel(" • " + "False"));
                }
                
                panel.add(new JLabel("   "));
                panel.add(new JLabel("Tiempo usado: "+question.getUsedTime()));
            }
            else if(question.getType().equals(QuestionType.ShortAnswer)){

                panel.add(new JLabel("   "));
                panel.add(new JLabel("Respuestas correctas"));

                for(int j = 0; j < question.getCorrectAnswersSA().size(); j++){
                    AnswerSA answer = (AnswerSA)question.getCorrectAnswersSA().get(j);
                    panel.add(new JLabel(" • " + answer.getAnswer()));
                }

                panel.add(new JLabel("   "));
                panel.add(new JLabel("Respuesta marcada"));

                AnswerSA answer2 = (AnswerSA)question.getMarkedAnswerSA();      
                panel.add(new JLabel(" • "+ answer2.getAnswer()));
                
                panel.add(new JLabel("   "));
                panel.add(new JLabel("Tiempo usado: "+ question.getUsedTime() ));
            }
        }
        
        panel.add(new JLabel("   "));
        panel.add(new JLabel("PROMEDIO TIEMPO USADO: "+(promedio / questions.size())));
        
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        
        this.setLayout(new BorderLayout());
        
        this.setPreferredSize(new Dimension(500, 500));
    
        this.setVisible(true);
        
        JScrollPane panelPane = new JScrollPane(panel);    
        panelPane.setVisible(true);
        
        this.add(panelPane);
    }
    

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );

        setAutoscrolls(true);
        setMaximumSize(new java.awt.Dimension(11111100, 11111100));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 770, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 530, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel jPanel1;
    // End of variables declaration//GEN-END:variables
}
