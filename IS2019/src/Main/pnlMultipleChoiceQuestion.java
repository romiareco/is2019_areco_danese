/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import entities.Answer;
import entities.AnswerMC;
import entities.Question;
import entities.QuestionMC;
import entities.QuestionResult;
import entities.QuestionType;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.Timer;

/**
 *
 * @author romina.areco
 */
public class PnlMultipleChoiceQuestion extends javax.swing.JPanel {

    QuestionMC question;
    Timer timer;
    Boolean expiredTime = false;
    int counterTime;
    /**
     * Creates new form pnlMultipleChoiceQuestion
     */
    public PnlMultipleChoiceQuestion(QuestionMC q) {
        initComponents();
        
        expiredTime = false;
        
        question=q;
        
        if(q.getQuestion().length() > 85){
            lblQuestion.setText("<HTML> "+q.getQuestion().substring(0, 80)+"<BR />"+ q.getQuestion().substring(80)+"</HTML>");
        }
        else{
            lblQuestion.setText(q.getQuestion());
        }
        for(int i = 1; i <= q.getAnswers().size(); i++){
        
            AnswerMC answer = q.getAnswers().get(i-1);
            if(i==1) chkAnswer1.setText(answer.getAnswer());
            if(i==2) chkAnswer2.setText(answer.getAnswer());
            if(i==3) chkAnswer3.setText(answer.getAnswer());
            if(i==4) chkAnswer4.setText(answer.getAnswer());
            if(i==5) chkAnswer5.setText(answer.getAnswer());
            if(i==6) chkAnswer6.setText(answer.getAnswer());
            
            chkAnswer1.setEnabled(true);
            chkAnswer2.setEnabled(true);
            chkAnswer3.setEnabled(true);
            chkAnswer4.setEnabled(true);
            chkAnswer5.setEnabled(true);
            chkAnswer6.setEnabled(true);
        }
        
        for(int i = q.getAnswers().size(); i <= 6; i++){
            if(i==0) chkAnswer1.setVisible(false);
            if(i==1) chkAnswer2.setVisible(false);
            if(i==2) chkAnswer3.setVisible(false);
            if(i==3) chkAnswer4.setVisible(false);
            if(i==4) chkAnswer5.setVisible(false);
            if(i==5) chkAnswer6.setVisible(false);
        }
        
        
        jprogressTime.setValue(100);
        ActionListener listener = new ActionListener() {
             
            public void actionPerformed(ActionEvent ae) {
                
                counterTime++;
                jprogressTime.setValue(100 - (counterTime * 100 / question.getMaxTime()));
                if (jprogressTime.getValue()<1) {
                    JOptionPane.showMessageDialog(null, "Ups! Expiro el tiempo");
                    
                    expiredTime = true;
                    
                    for(int i = 1; i <= q.getAnswers().size(); i++){
        
                        chkAnswer1.setEnabled(false);
                        chkAnswer2.setEnabled(false);
                        chkAnswer3.setEnabled(false);
                        chkAnswer4.setEnabled(false);
                        chkAnswer5.setEnabled(false);
                        chkAnswer6.setEnabled(false);
                    }
                    
                    timer.stop();
                } 
            }
        };
        timer = new Timer(1000, listener);
        timer.start();
        
        
    }
    
    public ArrayList<AnswerMC> getCorrectAnswers(){
        
        if(expiredTime){
            return null;
        }
        
        ArrayList<AnswerMC> validAnswers = new ArrayList<>();
         
        for(int i = 0; i < question.getAnswers().size(); i++){
            if(question.getAnswers().get(i).isValid()){
                validAnswers.add(question.getAnswers().get(i));
            }
        }
        
        return validAnswers;
    }
    
    
     public QuestionResult getQuestionResult(){
        
        timer.stop();
         
        QuestionResult result = new QuestionResult();
        result.setCorrectAnswers(getCorrectAnswers());
        result.setMarkedAnswers(getSelectedAnswers());
        result.setQuestion(question.getQuestion());
        result.setIdQuestion(question.getIdQuestion());
        result.setType(QuestionType.MultipleChoice);
        result.setTime(question.getMaxTime());
        result.setIsValid(false);
       
        
        if(getCorrectAnswers() != null){
            
            for(int i = 0; i< getSelectedAnswers().size(); i++){
                AnswerMC answer = getSelectedAnswers().get(i);
                if(answer.isValid()){
                    result.setIsValid(true);
                }
                
                chkAnswer1.setEnabled(false);
                chkAnswer2.setEnabled(false);
                chkAnswer3.setEnabled(false);
                chkAnswer4.setEnabled(false);
                chkAnswer5.setEnabled(false);
                chkAnswer6.setEnabled(false);
        
            }
        }
        
      
        int usedTime = (100 - jprogressTime.getValue()) *  question.getMaxTime()/ 100;
       
        result.setUsedTime(usedTime);

        return result;
    }
   
    public ArrayList<AnswerMC> getSelectedAnswers(){
        
        if(expiredTime){
            return null;
        }
         
        ArrayList<AnswerMC> result = new ArrayList<AnswerMC>();
        
        if(chkAnswer1.isSelected()){
            result.add(question.getAnswers().get(0));
        }
        if(chkAnswer2.isSelected()){
            result.add(question.getAnswers().get(1));
        }
        if(chkAnswer3.isSelected()){
            result.add(question.getAnswers().get(2));
        }
        if(chkAnswer4.isSelected()){
            result.add(question.getAnswers().get(3));
        }
        if(chkAnswer5.isSelected()){
            result.add(question.getAnswers().get(4));
        }
        if(chkAnswer6.isSelected()){
            result.add(question.getAnswers().get(5));
        }
        
        return result;
    }

    private PnlMultipleChoiceQuestion() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public int getUsedTime(){
        return jprogressTime.getValue();
    }
    
    public Boolean isExpired(){
        return expiredTime;
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblQuestion = new javax.swing.JLabel();
        jprogressTime = new javax.swing.JProgressBar();
        jSeparator1 = new javax.swing.JSeparator();
        chkAnswer1 = new javax.swing.JCheckBox();
        chkAnswer2 = new javax.swing.JCheckBox();
        chkAnswer3 = new javax.swing.JCheckBox();
        chkAnswer4 = new javax.swing.JCheckBox();
        chkAnswer5 = new javax.swing.JCheckBox();
        chkAnswer6 = new javax.swing.JCheckBox();

        lblQuestion.setFont(new java.awt.Font("Lucida Grande", 0, 14)); // NOI18N
        lblQuestion.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblQuestion.setText("Pregunta uno??? O dos?");
        lblQuestion.setMaximumSize(new java.awt.Dimension(60, 70));

        chkAnswer1.setText("jCheckBox1");

        chkAnswer2.setText("jCheckBox1");

        chkAnswer3.setText("jCheckBox1");

        chkAnswer4.setText("jCheckBox1");

        chkAnswer5.setText("jCheckBox1");

        chkAnswer6.setText("jCheckBox1");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(lblQuestion, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(22, 22, 22)
                        .addComponent(jprogressTime, javax.swing.GroupLayout.PREFERRED_SIZE, 667, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18))
            .addGroup(layout.createSequentialGroup()
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 602, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(chkAnswer1, javax.swing.GroupLayout.PREFERRED_SIZE, 391, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(chkAnswer3, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(chkAnswer2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 380, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(chkAnswer4, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(chkAnswer5, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 380, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(chkAnswer6, javax.swing.GroupLayout.PREFERRED_SIZE, 380, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(143, 143, 143))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(jprogressTime, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblQuestion, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 6, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(chkAnswer1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(chkAnswer2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(chkAnswer3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(chkAnswer4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(chkAnswer5)
                .addGap(2, 2, 2)
                .addComponent(chkAnswer6, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(12, 12, 12))
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JCheckBox chkAnswer1;
    private javax.swing.JCheckBox chkAnswer2;
    private javax.swing.JCheckBox chkAnswer3;
    private javax.swing.JCheckBox chkAnswer4;
    private javax.swing.JCheckBox chkAnswer5;
    private javax.swing.JCheckBox chkAnswer6;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JProgressBar jprogressTime;
    private javax.swing.JLabel lblQuestion;
    // End of variables declaration//GEN-END:variables
}
