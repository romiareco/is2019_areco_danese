/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.util.ArrayList;

/**
 *
 * @author romina.areco
 */
public class Game {
 
    ArrayList<Question> questions;
    ArrayList<Answer> answers;

    public Game() {
    }

    public Game(ArrayList<Question> questions, ArrayList<Answer> answers) {
        this.questions = questions;
        this.answers = answers;
    }

    public ArrayList<Answer> getAnswers() {
        return answers;
    }

    public ArrayList<Question> getQuestions() {
        return questions;
    }

    public void setAnswers(ArrayList<Answer> answers) {
        this.answers = answers;
    }

    public void setQuestions(ArrayList<Question> questions) {
        this.questions = questions;
    }
    
    
}
