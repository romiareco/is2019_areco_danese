/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.util.ArrayList;
import javax.swing.Icon;

/**
 *
 * @author romina.areco
 */

public class Question {
    
    private String idQuestion;
    private String question;
    
    private int maxTime;
    private QuestionType type;
    
    private Boolean resultIsCorrect;

    public Question() {
    }

    public Question(String idQuestion, String question, int maxTime, QuestionType type) {
        this.idQuestion = idQuestion;
        this.question = question;
        this.maxTime = maxTime;
        this.type = type;
    }
    
    public String getIdQuestion() {
        return idQuestion;
    }


    public Boolean getResultIsCorrect() {
        return resultIsCorrect;
    }

    public void setResultIsCorrect(Boolean resultIsCorrect) {
        this.resultIsCorrect = resultIsCorrect;
    }
    
    public int getMaxTime() {
        return maxTime;
    }
    public String getQuestion() {
        return question;
    }

    public void setIdQuestion(String idQuestion) {
        this.idQuestion = idQuestion;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public void setMaxTime(int maxTime) {
        this.maxTime = maxTime;
    }

    public void setType(QuestionType type) {
        this.type = type;
    }

    public QuestionType getType() {
        return type;
    }
}
    
