/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.util.ArrayList;
import javax.swing.Icon;

/**
 *
 * @author romina.areco
 */

public class QuestionMC extends Question {
    
    private ArrayList<AnswerMC> answers;
    private int usedTime;
    
    public QuestionMC() {
    }

    public QuestionMC(ArrayList<AnswerMC> answers, int usedTime) {
        this.answers = answers;
        this.usedTime = usedTime;
    }

    public QuestionMC(ArrayList<AnswerMC> answers, int usedTime, String idQuestion, String question, int maxTime, QuestionType type) {
        super(idQuestion, question, maxTime, type);
        this.answers = answers;
        this.usedTime = usedTime;
    }

   
  
    
    public void setAnswers(ArrayList<AnswerMC> answers) {
        this.answers = answers;
    }

    public ArrayList<AnswerMC> getAnswers() {
        return answers;
    }

    public int getUsedTime() {
        return usedTime;
    }

    public void setUsedTime(int usedTime) {
        this.usedTime = usedTime;
    }
    
    

    
    
    

}
    
