/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.util.ArrayList;
import javax.swing.Icon;

/**
 *
 * @author romina.areco
 */

public class QuestionTF extends Question {
    
    private AnswerTF answer;
    private int usedTime;
    
    
    public QuestionTF() {
    }

    public QuestionTF(AnswerTF answer, int usedTime) {
        this.answer = answer;
        this.usedTime = usedTime;
    }

    public QuestionTF(AnswerTF answer, int usedTime, String idQuestion, String question, int maxTime, QuestionType type) {
        super(idQuestion, question, maxTime, type);
        this.answer = answer;
        this.usedTime = usedTime;
    }

   

   
   
    public AnswerTF getAnswer() {
        return answer;
    }

    public void setAnswer(AnswerTF answer) {
        this.answer = answer;
    }

    public int getUsedTime() {
        return usedTime;
    }

    public void setUsedTime(int usedTime) {
        this.usedTime = usedTime;
    }
    
    
    

}
    
