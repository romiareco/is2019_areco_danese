/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

/**
 *
 * @author romina.areco
 */
public class Answer {
    
    private int idAnswer;
    private boolean valid;

    public Answer() {
    }

    public Answer(int idAnswer, boolean valid) {
        this.idAnswer = idAnswer;
        this.valid = valid;
    }

    public int getIdAnswer() {
        return idAnswer;
    }
   
    public void setIdAnswer(int idAnswer) {
        this.idAnswer = idAnswer;
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    
   
    
    
}
