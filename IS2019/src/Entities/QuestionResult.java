/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.util.ArrayList;

/**
 *
 * @author romina.areco
 */
public class QuestionResult {
    
    private String idQuestion;
    private Boolean isValid;
    private QuestionType type;
    private String Question;
    
    private int usedTime;
    private int time;
    
    private ArrayList<AnswerMC> markedAnswers;
    private ArrayList<AnswerMC> correctAnswers;
    
    
    private AnswerSA markedAnswerSA;
    private ArrayList<AnswerSA> correctAnswersSA;
    
    private AnswerTF markedAnswer;
    private AnswerTF correctAnswer;

    public void setCorrectAnswer(AnswerTF correctAnswer) {
        this.correctAnswer = correctAnswer;
    }

    public String getQuestion() {
        return Question;
    }

    public void setQuestion(String Question) {
        this.Question = Question;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }
    
    

    public void setUsedTime(int usedTime) {
        this.usedTime = usedTime;
    }

    public int getUsedTime() {
        return usedTime;
    }

    
    public void setCorrectAnswers(ArrayList<AnswerMC> correctAnswers) {
        this.correctAnswers = correctAnswers;
    }
    
    public void setCorrectAnswersSA(ArrayList<AnswerSA> correctAnswers) {
        this.correctAnswersSA = correctAnswers;
    }

    public ArrayList<AnswerSA> getCorrectAnswersSA() {
        return correctAnswersSA;
    }

    public AnswerSA getMarkedAnswerSA() {
        return markedAnswerSA;
    }

    public void setMarkedAnswerSA(AnswerSA markedAnswerSA) {
        this.markedAnswerSA = markedAnswerSA;
    }

   

     
    public void setIdQuestion(String idQuestion) {
        this.idQuestion = idQuestion;
    }

    public void setIsValid(Boolean isValid) {
        this.isValid = isValid;
    }

    public void setMarkedAnswer(AnswerTF markedAnswer) {
        this.markedAnswer = markedAnswer;
    }

    public void setMarkedAnswers(ArrayList<AnswerMC> markedAnswers) {
        this.markedAnswers = markedAnswers;
    }

    public void setType(QuestionType type) {
        this.type = type;
    }

    public AnswerTF getCorrectAnswer() {
        return correctAnswer;
    }

    public ArrayList<AnswerMC> getCorrectAnswers() {
        return correctAnswers;
    }

    public String getIdQuestion() {
        return idQuestion;
    }

    public Boolean getIsValid() {
        return isValid;
    }

    public AnswerTF getMarkedAnswer() {
        return markedAnswer;
    }

    public ArrayList<AnswerMC> getMarkedAnswers() {
        return markedAnswers;
    }

 
    public QuestionType getType() {
        return type;
    }
    
            
}
