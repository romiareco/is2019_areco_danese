/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.util.ArrayList;
import javax.swing.Icon;

/**
 *
 * @author romina.areco
 */

public class QuestionSA extends Question {
    
    private ArrayList<AnswerSA> answers;
    private int usedTime;
    
    public QuestionSA() {
    }

    public QuestionSA(ArrayList<AnswerSA> answers, int usedTime) {
        this.answers = answers;
        this.usedTime = usedTime;
    }

    public QuestionSA(ArrayList<AnswerSA> answers, int usedTime, String idQuestion, String question, int maxTime, QuestionType type) {
        super(idQuestion, question, maxTime, type);
        this.answers = answers;
        this.usedTime = usedTime;
    }

    
  
    
    public void setAnswers(ArrayList<AnswerSA> answers) {
        this.answers = answers;
    }

    public ArrayList<AnswerSA> getAnswers() {
        return answers;
    }

    public int getUsedTime() {
        return usedTime;
    }

    public void setUsedTime(int usedTime) {
        this.usedTime = usedTime;
    }
    
    

    
    
    

}
    
