/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

/**
 *
 * @author romina.areco
 */
public class AnswerSA extends Answer {
    
    private String answer;

    public AnswerSA(String answer) {
        this.answer = answer;
    }

    public AnswerSA(String answer, int idAnswer, boolean valid) {
        super(idAnswer, valid);
        this.answer = answer;
    }

  

    public AnswerSA() {
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    
    
   
    
    
}
