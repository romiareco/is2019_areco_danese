/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

/**
 *
 * @author romina.areco
 */
public class AnswerTF extends Answer {
    
    private Boolean answer;

    public AnswerTF() {
    }

    public AnswerTF(Boolean answer) {
        this.answer = answer;
    }

    public AnswerTF(Boolean answer, int idAnswer, boolean valid) {
        super(idAnswer, valid);
        this.answer = answer;
    }
  
    public Boolean getAnswer() {
        return answer;
    }

    public void setAnswer(Boolean answer) {
        this.answer = answer;
    }

    

     
    
}
