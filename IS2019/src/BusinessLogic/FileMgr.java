/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package businesslogic;

import entities.Answer;
import entities.AnswerMC;
import entities.AnswerSA;
import entities.AnswerTF;
import entities.Question;
import entities.QuestionMC;
import entities.QuestionSA;
import entities.QuestionTF;
import entities.QuestionType;
import java.io.File;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author romina.areco
 */
public class FileMgr {
    
    public static ArrayList leerArchivo(File file) throws java.io.FileNotFoundException {
       
        ArrayList<Question> questionList = new ArrayList<>();	
     
        Scanner scanner = new Scanner(file);

        while (scanner.hasNextLine()) 
        {
            String line = scanner.nextLine();
          
            if(!line.trim().isEmpty()){
              
                int time = 30;

                if(line.contains("//")){

                    line = line.replace("/", " ").trim();

                    if(line!=null){
                        time = Integer.parseInt(line);
                    }

                    line = scanner.nextLine();
                }

                String[] paraParsear = line.split("::");

                String questionId = paraParsear[1];

                String question = paraParsear[2].substring(0, paraParsear[2].indexOf("{"));
                String answers = paraParsear[2].substring(paraParsear[2].indexOf("{"));

                answers=answers.replace("}", "");
                answers=answers.replace("{", "");

                QuestionType type = QuestionType.TrueOrFalse;

                if(answers.contains("~")){
                    type = QuestionType.MultipleChoice;
                }
                else if (answers.contains("=")){
                    type = QuestionType.ShortAnswer;
                }

                switch(type){
                    case MultipleChoice:  

                        QuestionMC q= new QuestionMC();

                        q.setMaxTime(time);
                        q.setIdQuestion(questionId);
                        q.setQuestion(question);
                        q.setType(QuestionType.MultipleChoice);

                        ArrayList<AnswerMC> answerProcessed = new ArrayList<>();

                        String[] answerList = answers.split(",");

                        for(int i=0; i<answerList.length; i++){

                            AnswerMC a = new AnswerMC();
                            a.setIdAnswer(i);

                            if(answerList[i].contains("=")){
                                String answer = answerList[i];

                                answer = answer.split("#")[0];
                                a.setAnswer(answer.replace("=", ""));
                                a.setValid(true);

                            }
                            else {
                                String answer = answerList[i];

                                answer = answer.split("#")[0];
                                a.setAnswer(answer.replace("~", ""));
                                a.setValid(false);                                          
                            }

                            answerProcessed.add(a);

                        }
                        q.setAnswers(answerProcessed);
                        questionList.add(q);
                        break;
                    case ShortAnswer: 
                        
                        QuestionSA qSA= new QuestionSA();

                        qSA.setMaxTime(time);
                        qSA.setIdQuestion(questionId);
                        qSA.setQuestion(question);
                        qSA.setType(QuestionType.ShortAnswer);

                        ArrayList<AnswerSA> answerProcessedSA = new ArrayList<>();

                        String[] answerListSA = answers.split(",");

                        for(int i=0; i<answerListSA.length; i++){

                            AnswerSA a = new AnswerSA();
                            a.setIdAnswer(i);

                            if(answerListSA[i].contains("=")){
                                String answer = answerListSA[i];

                                answer = answer.split("#")[0];
                                a.setAnswer(answer.replace("=", ""));
                                a.setValid(true);

                            }
                            else {
                                String answer = answerListSA[i];

                                answer = answer.split("#")[0];
                                a.setAnswer(answer.replace("~", ""));
                                a.setValid(false);                                          
                            }

                            answerProcessedSA.add(a);

                        }
                        
                        qSA.setAnswers(answerProcessedSA);
                        questionList.add(qSA);
                        break;
                    case TrueOrFalse:
                        
                        QuestionTF qTF = new QuestionTF();

                        qTF.setMaxTime(time);
                        qTF.setIdQuestion(questionId);
                        qTF.setQuestion(question);
                        qTF.setType(QuestionType.TrueOrFalse);

                        AnswerTF answerProcessedTF = new AnswerTF();

                        if(answers.contains("T")){
                            answerProcessedTF.setAnswer(true);
                            answerProcessedTF.setValid(true);
                            answerProcessedTF.setIdAnswer(1);
                        }
                        else{
                            answerProcessedTF.setAnswer(false);
                            answerProcessedTF.setValid(true);
                            answerProcessedTF.setIdAnswer(1);
                        }
                        qTF.setAnswer(answerProcessedTF);
                        questionList.add(qTF);
                        
                        break;
                }     
            }
        }
        //se cierra el ojeto scanner
        scanner.close();

        return questionList;
    }

}
